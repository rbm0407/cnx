package conexao;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {
    
    public Connection con1;
    public Statement st1;
    
    public Conexao() throws SQLException {
        try {
            Class.forName("org.gjt.mm.mysql.Driver");
            con1 = DriverManager.getConnection("jdbc:mysql://localhost:3306/bancodedados", "usuario", "senha"); // @TODO inserir valores do seu banco
            st1 = con1.createStatement();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void enviaSQL(String sql){
        try {
            st1.execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ResultSet executaConsulta(String sql){
        ResultSet rs1 = null;
        try {
            rs1 = st1.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs1;
    }
    
    public void close(){
        try {
            st1.close();
            con1.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
